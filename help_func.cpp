#include <iostream>
#include <vector>
#include <unordered_set>
#include "help_func.h"
#include <random>

void set_print(const std::unordered_set<int> &set) {
    std::cout << "SET: { ";
    for (int v_el : set) {
        std::cout << v_el << " ";
    }
    std::cout << "}" << std::endl;
}

void randomize(std::unordered_set<int> &set, int count, int min, int max) {
    // First create an instance of an engine.
    std::random_device rnd_device;
    // Specify the engine and distribution.
    std::mt19937 mersenne_engine{rnd_device()};  // Generates random integers
    std::uniform_int_distribution<int> dist{min, max};

    auto gen = [&dist, &mersenne_engine]() {
        return dist(mersenne_engine);
    };

    set.clear();
    for (int i = 0; i < count; ++i) {
        set.insert(gen());
    }
}

void save_data(const time_data_map &time_data) {
    FILE *f = fopen("data.txt", "w");
    printf("\nSaving data..");
    for (const auto &threads_time_data: time_data) {
        fprintf(f, "%d ", threads_time_data.first);
        std::map<Algorithm, long> results;
        for (const auto &alg_time_data : threads_time_data.second) {
            fprintf(f, " %s [", GetAlgorithmName(alg_time_data.first).c_str());
            long avg_duration = 0;
            int iter = 0;
            for (const auto &duration: alg_time_data.second) {
                fprintf(f, "%ld, ", duration.second);
                avg_duration += duration.second;
                iter++;
            }
            avg_duration /= iter;
            results[alg_time_data.first] = avg_duration;
            fprintf(f, "]");
        }
        float pthread_boost = (float) results[Algorithm::BASE] / (float) results[Algorithm::PTHREAD];
        float openmp_boost = (float) results[Algorithm::BASE] / (float) results[Algorithm::OPENMP];
        fprintf(f, " pthread_boost: %.2f", pthread_boost);
        fprintf(f, " openmp_boost: %.2f", openmp_boost);
        fprintf(f, "\n");
    }

    fclose(f);
}