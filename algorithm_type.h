#ifndef PARALLEL_ALGORITHM_TYPE_H
#define PARALLEL_ALGORITHM_TYPE_H

#include <string>

enum class Algorithm {
    BASE, PTHREAD, OPENMP
};

std::string GetAlgorithmName(Algorithm algorithm);

#endif //PARALLEL_ALGORITHM_TYPE_H
