#include <vector>
#include <pthread.h>
#include <chrono>
#include <unordered_set>
#include "help_func.h"
#include "intersections/intersection_pthread.h"
#include "intersections/intersection_base.h"
#include "intersections/intersection_openmp.h"
#include "time_stats.h"
#include "algorithm_type.h"

#define ITERATIONS_COUNT 3

#define SET_MAX_SIZE 10000
#define MIN_RND_VALUE -10000
#define MAX_RMD_VALUE 10000

#define PRINT_SETS false

using namespace std::chrono;

const Algorithm ALGORITHMS[] = {Algorithm::BASE, Algorithm::PTHREAD, Algorithm::OPENMP};
const int THREADS[] = {1, 2, 4, 8, 16};

int main() {
    // Input sets with example values
    std::unordered_set<int> set1 = {1, 4, 3, 5, 35, 8, 7, 15, 0};
    std::unordered_set<int> set2 = {4, 5, 6, 7, 99, 1};
    std::unordered_set<int> set3 = {7, 5, 35, 43, 1};

    // Results data structures initializing
    std::unordered_set<int> result_base;
    std::unordered_set<int> result_pthread;
    std::unordered_set<int> result_openmp;

    // Main loop with time capture of observed algorithm
    time_data_map time_data;
    for (int i = 0; i < ITERATIONS_COUNT; ++i) {
        printf("\n < Iteration %d >\n", i + 1);
        // Randomizing input sets values
        randomize(set1, SET_MAX_SIZE, MIN_RND_VALUE, MAX_RMD_VALUE);
        randomize(set2, SET_MAX_SIZE, MIN_RND_VALUE, MAX_RMD_VALUE);
        randomize(set3, SET_MAX_SIZE, MIN_RND_VALUE, MAX_RMD_VALUE);

        // Input sets display
        if (PRINT_SETS) {
            set_print(set1);
            set_print(set2);
            set_print(set3);
        }

        // Working data structures initializing
        std::vector<int> vector1(set1.begin(), set1.end());
        std::vector<int> vector2(set2.begin(), set2.end());
        std::vector<int> vector3(set3.begin(), set3.end());

        for (auto tread_count: THREADS) {
            for (auto algorithm:ALGORITHMS) {
                void (*algorithm_func)(std::vector<int> &v1,
                                       std::vector<int> &v2, std::vector<int> &v3,
                                       std::unordered_set<int> &result,
                                       [[maybe_unused]] int threads_count);
                std::unordered_set<int> (*result);

                // Selecting algorithm
                switch (algorithm) {
                    case Algorithm::BASE:
                        algorithm_func = intersection_base;
                        result = &result_base;
                        break;
                    case Algorithm::PTHREAD:
                        algorithm_func = intersection_pthread;
                        result = &result_pthread;
                        break;
                    case Algorithm::OPENMP:
                        algorithm_func = intersection_openmp;
                        result = &result_openmp;
                        break;
                }
                result->clear();

                // Execution
                auto start = high_resolution_clock::now();
                algorithm_func(vector1, vector2, vector3, *result, tread_count);
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<milliseconds>(stop - start);

                // Saving time data
                time_data[tread_count][algorithm][i] = duration.count();
                printf("\n[%d] Time taken by function: %ld ms", i + 1, time_data[tread_count][algorithm][i]);
                if (PRINT_SETS) {
                    printf("\nResult -> ");
                    set_print(*result);
                }
                printf("-------------------------------------------------------------");
            }

            // Results comparison
            if (result_base != result_pthread) {
                printf("\n[ERROR] result_base != result_pthread");
            } else if (result_base != result_openmp) {
                printf("\n[ERROR] result_base != result_openmp");
            } else {
                printf("\nAll results are equal!\n");
            }
        }
    }

    printf("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    printf("\nAll intersection tests are successfully completed! Great job!");
    printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    // Time statistic calculations
    time_analyze(time_data);

    // Writing data toi file
    save_data(time_data);

    pthread_exit(nullptr);
}