#include "time_stats.h"
#include "algorithm_type.h"

#define SEQUENCE_AMOUNT 0.04

void time_analyze(const time_data_map &time_data) {
    printf("\n=========================== STATS ===========================\n");

    //float max_boost = 1.0 / (SEQUENCE_AMOUNT + (1.0 - SEQUENCE_AMOUNT) / 16.0);
    //printf("Amdahl's law: max boost -> %.4f\n\n", max_boost);

    for (const auto &threads_time_data: time_data) {
        printf("Testing with %d threads", threads_time_data.first);

        std::map<Algorithm, long> results;
        for (const auto &alg_time_data : threads_time_data.second) {
            long avg_duration = 0;
            int iter = 0;
            for (const auto &duration: alg_time_data.second) {
                avg_duration += duration.second;
                iter++;
            }
            avg_duration /= iter;
            results[alg_time_data.first] = avg_duration;
            printf("\n[%s] Average duration: %ld ms",
                   GetAlgorithmName(alg_time_data.first).c_str(),
                   results[alg_time_data.first]);
        }

        float pthread_boost = (float) results[Algorithm::BASE] / (float) results[Algorithm::PTHREAD];
        float openmp_boost = (float) results[Algorithm::BASE] / (float) results[Algorithm::OPENMP];
        printf("\nPTHREAD boost is %.4f", pthread_boost);
        printf("\nOPENMP boost is %.4f", openmp_boost);
        printf("\n-------------------------------------------------------------\n");
    }
}