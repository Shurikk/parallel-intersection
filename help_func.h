#ifndef PARALLEL_HELP_FUNC_H
#define PARALLEL_HELP_FUNC_H

#include "time_stats.h"

void set_print(const std::unordered_set<int> &set);

void vector_print(const std::vector<int> &vector);

void randomize(std::unordered_set<int> &set, int count, int min = 0, int max = 10);

void save_data(const time_data_map &time_data);

#endif //PARALLEL_HELP_FUNC_H
