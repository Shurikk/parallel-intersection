#ifndef PARALLEL_TIME_STATS_H
#define PARALLEL_TIME_STATS_H

#include <map>
#include "algorithm_type.h"

typedef std::map<int, std::map<Algorithm, std::map<int, long>>> time_data_map;

void time_analyze(const time_data_map &time_data);

#endif //PARALLEL_TIME_STATS_H
