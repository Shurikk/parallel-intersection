#include "algorithm_type.h"

std::string GetAlgorithmName(Algorithm algorithm) {
    switch (algorithm) {
        case Algorithm::BASE:
            return "BASE";
        case Algorithm::PTHREAD:
            return "PTHREAD";
        case Algorithm::OPENMP:
            return "OPENMP";
    }
}