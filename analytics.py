import matplotlib.pyplot as plt
import numpy as np
from typing import Dict, List

BASE = "BASE"
PTHREAD = "PTHREAD"
OPENMP = "OPENMP"

data: Dict[int, Dict[str, List[int]]] = dict()
pthread_boosts = []
openmp_boosts = []

# reading data from file
f = open("cmake-build-debug/data.txt", "r")
for line in f:
    base_data = line.split(BASE)[1]
    base_data = base_data[base_data.index("[") + 1:base_data.index("]")].split(", ")[:-1]
    pthread_data = line.split(PTHREAD)[1]
    pthread_data = pthread_data[pthread_data.index("[") + 1:pthread_data.index("]")].split(", ")[:-1]
    openmp_data = line.split(OPENMP)[1]
    openmp_data = openmp_data[openmp_data.index("[") + 1:openmp_data.index("]")].split(", ")[:-1]
    data[int(line[:line.index(" ")])] = {
        BASE: [int(x) for x in base_data],
        PTHREAD: [int(x) for x in pthread_data],
        OPENMP: [int(x) for x in openmp_data],
    }
    pthread_boost = line.split("pthread_boost: ")[1]
    pthread_boosts.append(float(pthread_boost[:pthread_boost.index(" ")]))
    openmp_boosts.append(float(line.split("openmp_boost: ")[1]))

threads = [th for th in data.keys()]
iterations = len(data[1][BASE])

# drawing all points
all_threads = list(np.repeat(threads, iterations))
all_pthread = [x for alg in data.values() for x in alg[PTHREAD]]
all_openmp = [x for alg in data.values() for x in alg[OPENMP]]

marker_size = 2
plt.plot(all_threads, all_pthread, 'o', color='green', markersize=marker_size)
plt.plot(all_threads, all_openmp, 'o', color='blue', markersize=marker_size)

# drawing avg lines
y_base = [np.average(alg[BASE]) for alg in data.values()]
y_pthread = [np.average(alg[PTHREAD]) for alg in data.values()]
y_openmp = [np.average(alg[OPENMP]) for alg in data.values()]

marker_size = 4
plt.plot(threads, y_base, label="base", linestyle='dashed', marker='o', markersize=marker_size, color='black')
plt.plot(threads, y_pthread, label="pthread", linestyle='dashed', marker='o', markersize=marker_size, color='green')
plt.plot(threads, y_openmp, label="openmp", linestyle='dashed', marker='o', markersize=marker_size, color='blue')

# plot display
plt.xlabel('threads')
plt.ylabel('time (ms)')
plt.title('Time')
plt.legend()
plt.show()

# drawing boosts
marker_size = 5
plt.plot(threads, pthread_boosts, label="pthread", linestyle='dashed', marker='o', markersize=marker_size,
         color='green')
plt.plot(threads, openmp_boosts, label="openmp", linestyle='dashed', marker='o', markersize=marker_size, color='blue')

# plot display
plt.xlabel('threads')
plt.ylabel('boost')
plt.title('Boost')
plt.legend()
plt.show()
