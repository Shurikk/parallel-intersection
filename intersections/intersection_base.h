#ifndef PARALLEL_INTERSECTION_BASE_H
#define PARALLEL_INTERSECTION_BASE_H

#include <vector>
#include <unordered_set>

void intersection_base(std::vector<int> &v1,
                       std::vector<int> &v2,
                       std::vector<int> &v3,
                       std::unordered_set<int> &result,
                       [[maybe_unused]] int threads_count = 1);


#endif //PARALLEL_INTERSECTION_BASE_H
