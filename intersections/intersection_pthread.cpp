#include <pthread.h>
#include "intersection_pthread.h"

pthread_mutex_t th_mutex;

struct arg_struct {
    arg_struct(int &threadsCount, int step, std::vector<int> &v1, std::vector<int> &v2, std::vector<int> &v3,
               std::unordered_set<int> &result)
            : threads_count(threadsCount), step(step), v1(v1), v2(v2), v3(v3), result(result) {}

    int &threads_count;
    int step;
    const std::vector<int> &v1;
    const std::vector<int> &v2;
    const std::vector<int> &v3;
    std::unordered_set<int> &result;
};


void *worker_thread(void *args) {
    auto *arguments = (struct arg_struct *) args;
    //printf("This is worker_thread (%d)\n", arguments->step);

    for (int i = arguments->step; i < arguments->v1.size(); i += arguments->threads_count) {
        bool has_same = false;
        for (int v2_el : arguments->v2) {
            if (arguments->v1[i] == v2_el) {
                for (int v3_el : arguments->v3) {
                    if (arguments->v1[i] == v3_el) {
                        has_same = true;
                        break;
                    }
                }
            } else {
                continue;
            }
            if (has_same) {
                break;
            }

        }
        if (has_same) {
            pthread_mutex_lock(&th_mutex);
            arguments->result.insert(arguments->v1[i]);
            pthread_mutex_unlock(&th_mutex);
        }
    }

    pthread_exit(nullptr);
}

void intersection_pthread(std::vector<int> &v1,
                          std::vector<int> &v2,
                          std::vector<int> &v3,
                          std::unordered_set<int> &result,
                          int threads_count) {
    printf("\n[PTHREAD] Searching for intersections..");

    pthread_t threads[threads_count];
    int td_counter = 0;

    std::vector<int> (*first_v) = &v1;
    std::vector<int> (*second_v) = &v2;
    std::vector<int> (*third_v) = &v3;

    if (v1.size() >= v2.size()) {
        if (v1.size() >= v3.size()) {
            first_v = &v1;
            second_v = &v2;
            third_v = &v3;
        } else {
            first_v = &v3;
            second_v = &v1;
            third_v = &v2;
        }
    } else if (v2.size() >= v3.size()) {
        first_v = &v2;
        second_v = &v1;
        third_v = &v3;
    }

    for (int step = 0; step < threads_count; step++) {
        auto *args = new arg_struct(threads_count, step, *first_v, *second_v, *third_v, result);
        int ret = pthread_create(&threads[td_counter], nullptr, &worker_thread, (void *) args);
        td_counter++;
        if (ret != 0) {
            printf("Error: pthread_create() failed\n");
            exit(EXIT_FAILURE);
        }
    }
    for (auto thread: threads) {
        pthread_join(thread, nullptr);
    }
}