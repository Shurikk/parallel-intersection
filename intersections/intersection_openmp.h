#ifndef PARALLEL_INTERSECTION_OPENMP_H
#define PARALLEL_INTERSECTION_OPENMP_H

#include <vector>
#include <unordered_set>

void intersection_openmp(std::vector<int> &v1,
                         std::vector<int> &v2,
                         std::vector<int> &v3,
                         std::unordered_set<int> &result,
                         int threads_count);

#endif //PARALLEL_INTERSECTION_OPENMP_H
