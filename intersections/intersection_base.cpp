#include "intersection_base.h"


void intersection_base(std::vector<int> &v1,
                       std::vector<int> &v2,
                       std::vector<int> &v3,
                       std::unordered_set<int> &result,
                       [[maybe_unused]] int threads_count) {
    printf("\n[BASE] Searching for intersections..");
    for (int v1_el : v1) {
        bool has_same = false;
        for (int v2_el : v2) {
            if (v1_el == v2_el) {
                for (int v3_el : v3) {
                    if (v1_el == v3_el) {
                        has_same = true;
                        break;
                    }
                }
            } else {
                continue;
            }
            if (has_same) {
                break;
            }
        }
        if (has_same) {
            result.insert(v1_el);
        }
    }
}
