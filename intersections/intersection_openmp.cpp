#include <omp.h>
#include "intersection_openmp.h"

void intersection_openmp(std::vector<int> &v1,
                         std::vector<int> &v2,
                         std::vector<int> &v3,
                         std::unordered_set<int> &result,
                         int threads_count) {
    printf("\n[OPENMP] Searching for intersections..");
    omp_set_num_threads(threads_count);
#pragma omp parallel for shared(result) shared(v1) shared(v2) shared(v3) default(none)
    for (int v1_el : v1) {
        bool has_same = false;
        for (int v2_el : v2) {
            if (v1_el == v2_el) {
                for (int v3_el : v3) {
                    if (v1_el == v3_el) {
                        has_same = true;
                        break;
                    }
                }
            } else {
                continue;
            }
            if (has_same) {
                break;
            }
        }
        if (has_same) {
#pragma omp critical
            result.insert(v1_el);
        }
    }
}
